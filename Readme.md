#Primordial

##Primordial

[![NuGet](https://img.shields.io/nuget/v/primordial.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial/)

Primordial soup of code.

## Primordial.Host

[![NuGet](https://img.shields.io/nuget/v/primordial.host.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.Host/)

## Primordial.Logging

[![NuGet](https://img.shields.io/nuget/v/primordial.logging.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.Logging/)

Simple logging abstraction.

## Primordial.Logging.NLog

[![NuGet](https://img.shields.io/nuget/v/primordial.logging.svg?maxAge=86400)](https://www.nuget.org/packages/Primordial.Logging.NLog/)

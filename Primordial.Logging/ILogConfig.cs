using System;

namespace Primordial.Logging
{
    [Flags]
    public enum LogTargets
    {
        Debug = 1,
        Console = 2,
        File = 4,
        Default = Console | File,
        All = Debug | Console | File
    }

    public interface ILogConfig
    {
        LogTargets LogTargets { get; }
        string LogFile { get; }
    }
}
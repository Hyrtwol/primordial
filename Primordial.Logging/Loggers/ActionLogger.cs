using System;

namespace Primordial.Logging.Loggers
{
    public class ActionLogger : LoggerBase
    {
        private readonly Action<string> _callback;

        public ActionLogger(Action<string> callback)
        {
            _callback = callback;
        }

        protected override void WriteLine(string message)
        {
            _callback(message);
        }
    }
}
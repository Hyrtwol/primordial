using System;

namespace Primordial.Logging.Loggers
{
    public class ConsoleLogger : LoggerBase
    {
        protected override void WriteLine(string message)
        {
            Console.WriteLine(message);
        }
    }
}
using System;

namespace Primordial.Logging.Loggers
{
    public abstract class LoggerBase : ILogger
    {
        protected abstract void WriteLine(string message);

        public virtual void Log(LogLevel level, object message)
        {
            WriteLine($"[{level,-5}] {message}");
        }

        public virtual void Log(LogLevel level, string format, params object[] arguments)
        {
            object message = arguments.Length > 0 ? string.Format(format, arguments) : format;
            Log(level, message);
        }

        public virtual void Log(Exception exception)
        {
            Log(LogLevel.Error, exception.Message);
        }
    }
}
using System;
using System.Diagnostics;
using System.IO.Pipes;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace Primordial.Logging.Loggers
{
    public class IPCLogger : LoggerBase
    {
        private static readonly Encoding Encoding = Encoding.UTF8;
        
        private NamedPipeClientStream _pipeClientStream;

        public IPCLogger()
        {
        }

        public string ServerName { get; set; } = ".";

        public string PipeName { get; set; } = "ipc-log";

        public int BufferSize { get; set; } = 4096;

        public int Timeout { get; set; } = 10;

        public bool KeepAlive { get; set; } = false;

        protected override void WriteLine(string message)
        {
            CreatePipe();
            if (_pipeClientStream == null) return;
            Connect();
            if (_pipeClientStream.IsConnected)
            {

                var buffer = Encoding.GetBytes(message);
                Debug.Assert(buffer.Length <= BufferSize);
                _pipeClientStream.Write(buffer, 0, buffer.Length);
            }
            else
            {
                Debug.WriteLine("Unable to connect to pipe " + PipeName);
            }
            if (!KeepAlive)
            {
                ClosePipe();
            }
        }
        
        private void CreatePipe()
        {
            if (_pipeClientStream != null) return;
            _pipeClientStream = new NamedPipeClientStream(
                ServerName,
                PipeName,
                PipeDirection.Out,
                PipeOptions.Asynchronous | PipeOptions.WriteThrough);
        }

        private void Connect()
        {
            if (_pipeClientStream == null) return;
            if (_pipeClientStream.IsConnected) return;
            _pipeClientStream.Connect(Timeout);
        }

        private void ClosePipe()
        {
            if (_pipeClientStream == null) return;
            _pipeClientStream.Dispose();
            _pipeClientStream = null;
        }
    }
}
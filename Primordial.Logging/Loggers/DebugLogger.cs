using System.Diagnostics;

namespace Primordial.Logging.Loggers
{
    public class DebugLogger : LoggerBase
    {
        protected override void WriteLine(string message)
        {
            Debug.WriteLine(message);
        }
    }
}
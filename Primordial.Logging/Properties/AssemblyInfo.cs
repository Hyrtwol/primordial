using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.Logging")]
[assembly: AssemblyDescription("Simple logging abstraction")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.Logging")]
[assembly: AssemblyCopyright("Copyright 2015 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("5fe9c4db-05cd-4302-ae2b-869666b60bd7")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]

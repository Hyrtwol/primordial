using System.IO;
using Primordial.Logging.Loggers;

namespace Primordial.Logging.Test.Loggers
{
    public class TextWriterLogger : LoggerBase
    {
        private readonly TextWriter _textWriter;

        public TextWriterLogger(TextWriter textWriter)
        {
            _textWriter = textWriter;
        }

        protected override void WriteLine(string message)
        {
            _textWriter.WriteLine(message);
        }
    }
}
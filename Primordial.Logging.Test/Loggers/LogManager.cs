namespace Primordial.Logging.Test.Loggers
{
    public class LogManager<T> : ILogManager where T: ILogger, new()
    {
        private readonly ILogConfig _config;

        public LogManager(ILogConfig config)
        {
            _config = config;
        }

        public ILogger GetLogger(string name)
        {
            var logger = new T();
            return logger;
        }
    }
}
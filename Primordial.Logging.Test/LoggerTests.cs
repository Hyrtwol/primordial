﻿using NUnit.Framework;
using Primordial.Logging.Loggers;
using Primordial.Logging.Test.Loggers;

namespace Primordial.Logging.Test
{
    [TestFixture]
    public class LoggerTests : ILogConfig, ILogManager
    {
        public LogTargets LogTargets => LogTargets.Debug;
        public string LogFile => "test.log";

        [Test]
        public void WriteToLog()
        {
            //ILogManager logManager = new LogManager<DebugLogger>(this);
            ILogManager logManager = this;
            var log = logManager.GetLogger("test");
            log.Debug("hello");
            log.Info("hello");
            log.Warn("hello");
            log.Error("hello");
        }

        public ILogger GetLogger(string name)
        {
            return new DebugLogger();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using NLog;
using NLog.Common;
using NLog.Config;
using NUnit.Framework;

namespace Primordial.Logging.NLog.Test
{
#if !DEBUG
    [Ignore("Depends on external endpoint")]
#endif
    [TestFixture]
    public class IPCTargetTests
    {
        [Test]
        public void LogErrorFileConfig()
        {
            var ll = global::NLog.LogLevel.Info;

            LogManager.Configuration = new XmlLoggingConfiguration("NLogTest.config");
            Debug.WriteLine(LogManager.Configuration);

            foreach (var target in LogManager.Configuration.AllTargets)
            {
                Debug.Print("target={0}", target);
            }

            foreach (var loggingRule in LogManager.Configuration.LoggingRules)
            {
                Debug.Print("loggingRule={0}", loggingRule);
            }

            var logger = LogManager.GetLogger("Test");
            Assert.IsNotNull(logger);

            for (int i = 1; i <= 1; i++)
            {
                try
                {
                    throw new NotSupportedException("Not a real exception! " + i);
                }
                catch (Exception ex)
                {
                    //Debug.WriteLine("Logging error");
                    logger.Error(ex, "Fake error #" + i);
                }
                Thread.Sleep(1);
            }
        }

        //[Test]
        //public void DoBinaryFormatter()
        //{
        //    BinaryFormatter _bf = new BinaryFormatter();

        //    LogEventInfo logEvent = new LogEventInfo(global::NLog.LogLevel.Info, 
        //        "AsyncMessage", "Hello!");
        //    logEvent.Properties.Add("pipe-state", (int)1);
        //    logEvent.Properties.Add("pipe-bytes", (int)100);

        //    var le = new LogEvent();
        //    le.Level = logEvent.Level.Ordinal;
        //    le.LoggerName = logEvent.LoggerName;
        //    le.Message = logEvent.Message;
        //    le.Properties = new Dictionary<object, object>(logEvent.Properties);


        //    using (var ms = new MemoryStream())
        //    {
        //        _bf.Serialize(ms, le);
        //        //InternalLogger.Log(global::NLog.LogLevel.Info, "ms " + ms.Length);
        //        Debug.WriteLine("ms " + ms.Length);
        //        //_pipeClientStream.Write(ms.GetBuffer(), 0, (int)ms.Length);
        //    }
        //}
    }

    //[Serializable]
    //public class LogEvent
    //{
    //    public int Level;
    //    public string LoggerName;
    //    public string Message;
    //    public Dictionary<object, object> Properties;
    //}
}

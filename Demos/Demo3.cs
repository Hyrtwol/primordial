﻿using System.Threading;
using Primordial.Host;
using Primordial.Logging;
using Primordial.Logging.NLog;

namespace Primordial.Demos
{
    static class Program
    {
        static void Main()
        {
            var config = new HostConfig("demo3.log");
            var logManager = new LogManagerForNLog(config);
            using (var service = new Demo3(logManager))
            {
                WindowsService.Run(service);
            }
        }
    }
    
    class Demo3 : IHostService
    {
        private readonly ILogger _logger;
        private readonly Demo3Worker _worker;

        public string Name { get; }

        public Demo3(ILogManager logManager)
        {
            Name = GetType().Name;
            _logger = logManager.GetLogger(Name);
            _logger.Info("Constructing service " + Name);
            _worker = new Demo3Worker(logManager);
        }

        public void Start()
        {
            _logger.Info("Starting service " + Name);
            _worker.Start();
        }

        public void Stop()
        {
            _logger.Info("Stopping service " + Name);
            _worker.Stop();
        }

        public void Dispose()
        {
            _logger.Info("Disposing service " + Name);
            _worker.Dispose();
        }

    }

    class Demo3Worker : Worker
    {
        private readonly ILogger _logger;

        public Demo3Worker(ILogManager logManager)
        {
            _logger = logManager.GetLogger<Demo3Worker>();
            _logger.Info("Constructing worker " + GetType().Name);
        }

        protected override void WorkTick()
        {
            _logger.Info("tick " + GetType().Name);
            Thread.Sleep(1000);
        }
    }
}

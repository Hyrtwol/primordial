﻿using System.Threading;
using Primordial.Host;
using Primordial.Logging;
using Primordial.Logging.NLog;

namespace Primordial.Demos
{
    static class Program
    {
        static void Main()
        {
            var config = new HostConfig("demo4.log");
            var logManager = new LogManagerForNLog(config);
            using (var service = new HostServiceWithWorkers(
                new Demo41Worker(logManager),
                new Demo42Worker(logManager)
                ))
            {
                WindowsService.Run(service);
            }
        }
    }

    class Demo41Worker : Worker
    {
        private readonly ILogger _logger;

        public Demo41Worker(ILogManager logManager)
        {
            _logger = logManager.GetLogger<Demo41Worker>();
            _logger.Info("Constructing worker " + GetType().Name);
        }

        protected override void WorkTick()
        {
            _logger.Info("tick " + Name);
            Thread.Sleep(1000);
        }
    }

    class Demo42Worker : Worker
    {
        private readonly ILogger _logger;

        public Demo42Worker(ILogManager logManager)
        {
            _logger = logManager.GetLogger<Demo41Worker>();
            _logger.Info("Constructing worker " + GetType().Name);
        }

        protected override void WorkTick()
        {
            _logger.Info("tick " + Name);
            Thread.Sleep(1200);
        }
    }
}

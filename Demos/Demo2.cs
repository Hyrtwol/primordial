﻿using Primordial.Host;
using Primordial.Logging;
using Primordial.Logging.NLog;

namespace Primordial.Demos
{
    static class Program
    {
        static void Main()
        {
            var config = new HostConfig("demo2.log");
            var logManager = new LogManagerForNLog(config);
            using (var service = new Demo2(logManager))
            {
                WindowsService.Run(service);
            }
        }
    }
    
    class Demo2 : IHostService
    {
        private readonly ILogger _logger;

        public string Name { get; }

        public Demo2(ILogManager logManager)
        {
            Name = GetType().Name;
            _logger = logManager.GetLogger(Name);
            _logger.Info("Constructing service " + Name);
        }

        public void Start()
        {
            _logger.Info("Starting service " + Name);
        }

        public void Stop()
        {
            _logger.Info("Stopping service " + Name);
        }

        public void Dispose()
        {
            _logger.Info("Disposing service " + Name);
        }

    }
}

﻿using Primordial.Logging;

namespace Primordial.Demos
{
    class HostConfig : ILogConfig
    {
        public HostConfig(string logFile, LogTargets logTargets = LogTargets.All)
        {
            LogFile = logFile;
            LogTargets = logTargets;
        }

        public LogTargets LogTargets { get; }

        public string LogFile { get; }
    }
}
﻿using System;
using Primordial.Host;

namespace Primordial.Demos
{
    static class Program
    {
        static void Main(string[] args)
        {
            using (var service = new Demo1())
            {
                WindowsService.Run(service);
            }
        }
    }

    class Demo1 : IHostService
    {
        public string Name { get; }

        public Demo1()
        {
            Name = GetType().Name;
            Console.WriteLine("Constructing service " + Name);
        }

        public void Start()
        {
            Console.WriteLine("Starting service " + Name);
        }

        public void Stop()
        {
            Console.WriteLine("Stopping service " + Name);
        }

        public void Dispose()
        {
            Console.WriteLine("Disposing service " + Name);
        }

    }
}

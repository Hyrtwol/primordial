﻿using System;
using System.Threading;
using Primordial.Host;
using Primordial.Logging;
using Primordial.Logging.NLog;

namespace Primordial.Demos
{
    static class Program
    {
        static void Main()
        {
            var config = new HostConfig("demo5.log");
            var logManager = new LogManagerForNLog(config);
            var queue = new ProducerConsumer<string>();
            using (var service = new HostServiceWithWorkers(
                new Demo51Worker(logManager, queue),
                new Demo52Worker(logManager, queue, "receiver 1"),
                new Demo52Worker(logManager, queue, "receiver 2")
                ))
            {
                WindowsService.Run(service);
            }
        }
    }

    class Demo51Worker : Worker
    {
        private readonly ProducerConsumer<string> _queue;
        private readonly ILogger _logger;
        private readonly Random _random;
        private int _tick;

        public Demo51Worker(ILogManager logManager, ProducerConsumer<string> queue)
        {
            _queue = queue;
            _logger = logManager.GetLogger<Demo51Worker>();
            _logger.Info("Constructing worker " + GetType().Name);
            _tick = 0;
            _random = new Random();
        }

        protected override void WorkTick()
        {
            _tick++;
            var rep = _random.Next(6) + 1;
            for (int i = 0; i < rep; i++)
            {
                var msg = "tick " + _tick + "." + i;
                _queue.Produce(msg);
                _logger.Info(Name + " queued '" + msg + "' " + _queue.QueueCount);
            }
            Thread.Sleep(1000);
        }
    }

    class Demo52Worker : Worker
    {
        private readonly ProducerConsumer<string> _queue;
        private readonly ILogger _logger;

        public override string Name { get; }

        public Demo52Worker(ILogManager logManager, ProducerConsumer<string> queue, string name)
        {
            _queue = queue;
            _logger = logManager.GetLogger<Demo51Worker>();
            _logger.Info("Constructing worker " + name);
            Name = name;
        }

        protected override void WorkTick()
        {
            var payload = _queue.Fetch(1000);
            if (payload != null)
            {
                _logger.Info(Name + " recieved '" + payload + "' " + _queue.QueueCount);
                Thread.Sleep(600);
            }
        }
    }
}

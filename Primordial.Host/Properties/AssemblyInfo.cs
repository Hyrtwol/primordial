using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.Host")]
[assembly: AssemblyDescription("Windows service host")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.Host")]
[assembly: AssemblyCopyright("Copyright 2015 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("1d276ffa-0728-4d84-8c0b-124d1b7edca0")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]

﻿using System;

namespace Primordial.Host
{
    public class WindowsServiceHost : IHost
    {
        private readonly IHostService _hostService;

        public WindowsServiceHost(IHostService hostService)
        {
            if (hostService == null) throw new ArgumentNullException(nameof(hostService));
            _hostService = hostService;
        }

        public void Run()
        {
            WindowsService.Run(_hostService);
        }
    }
}
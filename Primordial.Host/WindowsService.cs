﻿using System;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Threading;

namespace Primordial.Host
{
    public class WindowsService : ServiceBase
    {
        // Required designer variable.
        // ReSharper disable once FieldCanBeMadeReadOnly.Local
        private System.ComponentModel.IContainer components = null;

        // http://msdn.microsoft.com/en-us/library/system.serviceprocess.serviceinstaller(v=vs.110).aspx

        public static ConsoleColor ConsoleColor = ConsoleColor.Cyan;
        public static string ConsoleStopText = "Press ESC to stop";
        public static string ConsoleExitText = "Press ESC to exit";

        public static void Run(IHostService service)
        {
            var windowsService = new WindowsService(service);
            if (Environment.UserInteractive)
            {
                Console.Title = service.Name;
                ConsoleUtils.ConfigureConsole();

                windowsService.OnStart(null);

                Console.ForegroundColor = ConsoleColor;
                Console.WriteLine(ConsoleStopText);
                Console.ResetColor();
                Console.Title = service.Name + " - " + ConsoleStopText;
                ConsoleUtils.WaitForEscape();
                Console.Title = service.Name;

                windowsService.Stop();

                Console.ForegroundColor = ConsoleColor;
                Console.WriteLine(ConsoleExitText);
                Console.ResetColor();
                Console.Title = service.Name + " - " + ConsoleExitText;
                ConsoleUtils.WaitForEscape();
                Console.Title = service.Name;
            }
            else
            {
                Run(windowsService);
            }
        }

        private readonly IHostService _service;

        private WindowsService(IHostService service)
        {
            if(service == null) throw new ArgumentNullException(nameof(service));
            ServiceName = service.Name;
            _service = service;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        protected override void OnStart(string[] args)
        {
            _service.Start();
        }

        protected override void OnStop()
        {
            _service.Stop();
        }

    }
}

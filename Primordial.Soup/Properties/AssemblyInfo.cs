using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.Soup")]
[assembly: AssemblyDescription("Simple logging abstraction")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.Soup")]
[assembly: AssemblyCopyright("Copyright 2015 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("2b467dc1-a988-41f7-8fe2-dc8e860fa778")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]

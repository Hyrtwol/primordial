using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial.Logging.NLog")]
[assembly: AssemblyDescription("Simple logging abstraction")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial.Logging.NLog")]
[assembly: AssemblyCopyright("Copyright 2015 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("e9665de3-f834-4e11-a1f1-9c490793cace")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]

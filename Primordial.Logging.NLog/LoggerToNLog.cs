﻿using System;
using NLog;

namespace Primordial.Logging.NLog
{
    internal class LoggerToNLog : ILogger
    {
        private readonly Logger _logger;

        public LoggerToNLog(Logger logger)
        {
            _logger = logger;
        }

        public void Log(LogLevel level, string format, params object[] arguments)
        {
            var nlogLevel = global::NLog.LogLevel.FromOrdinal((int)level);
            if (arguments.Length == 0) _logger.Log(nlogLevel, format);
            else _logger.Log(nlogLevel, format, arguments);
        }

        public void Log(LogLevel level, object message)
        {
            var nlogLevel = global::NLog.LogLevel.FromOrdinal((int)level);
            _logger.Log(nlogLevel, message);
        }

        public void Log(Exception exception)
        {
            _logger.Log(global::NLog.LogLevel.Error, exception, exception.Message);
        }
    }
}
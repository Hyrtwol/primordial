﻿//#define BUFFER_PREALLOCATED
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Pipes;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using NLog;
using NLog.Common;
using NLog.Config;
using NLog.Targets;

namespace Primordial.Logging.NLog
{
    [Target("IPC")]
    public sealed class IPCTarget : TargetWithLayout
    {
        private static readonly Encoding Encoding = Encoding.UTF8;
        private readonly BinaryFormatter _bf = new BinaryFormatter();

        private NamedPipeClientStream _pipeClientStream;
#if BUFFER_PREALLOCATED
        private byte[] buffer;
#endif

        public string ServerName { get; set; } = ".";

        [RequiredParameter]
        public string PipeName { get; set; }

        public int BufferSize { get; set; } = 4096;

        public int Timeout { get; set; } = 10;

        public bool KeepAlive { get; set; } = false;

        protected override void InitializeTarget()
        {
            base.InitializeTarget();
#if BUFFER_PREALLOCATED
            buffer = new byte[BufferSize];
#endif
            if (KeepAlive)
            {
                CreatePipe();
                Connect();
            }
        }

        private void CreatePipe()
        {
            if (_pipeClientStream != null) return;
            _pipeClientStream = new NamedPipeClientStream(
                ServerName,
                PipeName,
                PipeDirection.Out,
                PipeOptions.Asynchronous | PipeOptions.WriteThrough);
        }

        private void Connect()
        {
            if (_pipeClientStream == null) return;
            if (_pipeClientStream.IsConnected) return;
            _pipeClientStream.Connect(Timeout);

            InternalLogger.Log(global::NLog.LogLevel.Info,
                "Pipe " + PipeName + " is " +
                (_pipeClientStream.IsConnected ? "connected" : "disconnected"));
        }

        protected override void CloseTarget()
        {
            ClosePipe();
            base.CloseTarget();
        }

        private void ClosePipe()
        {
            if (_pipeClientStream == null) return;
            _pipeClientStream.Dispose();
            _pipeClientStream = null;
        }

        protected override void FlushAsync(AsyncContinuation asyncContinuation)
        {
            base.FlushAsync(asyncContinuation);
        }

        protected override void Dispose(bool disposing)
        {
            Debug.WriteLine("IPCTarget.Dispose()");
            if(disposing) ClosePipe();
            base.Dispose(disposing);
        }

        protected override void Write(LogEventInfo logEvent)
        {
            if (logEvent == null) throw new ArgumentNullException(nameof(logEvent));
            CreatePipe();
            if (_pipeClientStream == null) return;
            Connect();
            if (_pipeClientStream.IsConnected)
            {
                try
                {
#if NOGO
                        var le = new LogEvent();
                        le.Level = logEvent.Level.Ordinal;
                        le.LoggerName = logEvent.LoggerName;
                        le.Message = logEvent.Message;
                        le.Properties = new Dictionary<object, object>(logEvent.Properties);

                        using (var ms = new MemoryStream())
                        {
                            _bf.Serialize(ms, le);
                            InternalLogger.Log(global::NLog.LogLevel.Info, "ms " + ms.Length);
                            //Debug.WriteLine("ms " + ms.Length);
                            _pipeClientStream.Write(ms.GetBuffer(), 0, (int)ms.Length);
                        }

                        //_bf.Serialize(_pipeClientStream, logEvent);
                        //_bf.Serialize(_pipeClientStream, le);
#else
#if BUFFER_PREALLOCATED
                        var message = Layout.Render(logEvent);
                        var cnt = Encoding.GetBytes(message, 0, message.Length, buffer, 0);
                        _pipeClientStream.Write(buffer, 0, cnt);
#else
                    var message = Layout.Render(logEvent);
                    var buffer = Encoding.GetBytes(message);
                    Debug.Assert(buffer.Length <= BufferSize);
                    _pipeClientStream.Write(buffer, 0, buffer.Length);
#endif

#endif
                }
                catch (Exception ex)
                {
                    //Console.WriteLine("Send ERROR: {0}", e.Message);
                    InternalLogger.Error(ex, "IPC send error: " + ex.Message);
                }
            }
            else
            {
                InternalLogger.Error("Unable to connect to pipe " + PipeName);
            }
            if (!KeepAlive)
            {
                ClosePipe();
            }
        }
    }
    
    //[Serializable]
    //public class LogEvent
    //{
    //    public int Level;
    //    public string LoggerName;
    //    public string Message;
    //    public Dictionary<object, object> Properties;
    //}
}

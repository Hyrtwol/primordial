﻿using Ninject.Modules;

namespace Cuisine.Toaster.NLog
{
    public class LoggingModule : NinjectModule
    {
        public override void Load()
        {
            Bind<ILogManager>()
                .To<LogManagerForNLog>()
                .InSingletonScope();
        }
    }
}
﻿using System;
using System.Diagnostics;
using NLog;
using NLog.Config;
using NLog.Targets;

namespace Primordial.Logging.NLog
{
    public class LogManagerForNLog : ILogManager
    {
        //const string DefaultLayout = @"${date:format=HH\:MM\:ss} ${logger} ${message}";
        private const string DefaultLayout = @"${message}";
        //private readonly ILogConfig _config;

        public LogManagerForNLog()
        {
        }

        public LogManagerForNLog(ILogConfig config)
        {
            if (LogManager.Configuration == null)
            {
                Configure(config);
            }
        }

        public ILogger GetLogger(string name)
        {
            //if (LogManager.Configuration == null) throw new InvalidOperationException("NLog is not configured.");
            return new LoggerToNLog(LogManager.GetLogger(name));
        }

        private void Configure(ILogConfig _config)
        {
            var logTargets = _config.LogTargets;
            //Debug.Print("Configuring NLog with {0}", logTargets);
            if (LogManager.Configuration != null) throw new InvalidOperationException("NLog is already configured.");
            var config = new LoggingConfiguration();
#if DEBUG
            if ((logTargets & LogTargets.Debug) > 0)
            {
                Debug.Print("Configuring NLog with diagnostics debug output");
                var debugTarget = new DiagnosticsDebugTarget();
                debugTarget.Layout = DefaultLayout;
                config.AddTarget("debugger", debugTarget);
                var rule1 = new LoggingRule("*", global::NLog.LogLevel.Debug, debugTarget);
                config.LoggingRules.Add(rule1);
            }
#endif
            if (Environment.UserInteractive && ((logTargets & LogTargets.Console) > 0))
            {
                Debug.Print("Configuring NLog with console output");
                //new ColoredConsoleTarget()
                var consoleTarget = new ColoredConsoleTarget();
                consoleTarget.Layout = DefaultLayout;
                config.AddTarget("console", consoleTarget);
                var rule1 = new LoggingRule("*", global::NLog.LogLevel.Debug, consoleTarget);
                config.LoggingRules.Add(rule1);
            }

            if ((logTargets & LogTargets.File) > 0)
            {
                var logFileName = _config.LogFile;
                if (!string.IsNullOrWhiteSpace(logFileName))
                {
                    logFileName = Environment.ExpandEnvironmentVariables(logFileName);
                    Debug.Print("Configuring NLog with file {0}", logFileName);
                    var fileTarget = new FileTarget();
                    fileTarget.FileName = logFileName;
                    fileTarget.Layout = DefaultLayout;
                    //fileTarget.Layout = new LogstashLayout(pattern, new[] { "correlation_id", "context_id" });
                    fileTarget.DeleteOldFileOnStartup = true;

                    config.AddTarget("file", fileTarget);
                    var rule2 = new LoggingRule("*", global::NLog.LogLevel.Debug, fileTarget);
                    config.LoggingRules.Add(rule2);
                }
            }

            // Step 5. Activate the configuration
            LogManager.Configuration = config;
        }

#if DEBUG
        [Target("DiagnosticsDebug")]
        private sealed class DiagnosticsDebugTarget : TargetWithLayout
        {
            protected override void Write(LogEventInfo logEvent)
            {
                if (logEvent == null) throw new ArgumentNullException(nameof(logEvent));
                var message = Layout.Render(logEvent);
                Debug.WriteLine(message);
            }
        }
#endif
    }
}

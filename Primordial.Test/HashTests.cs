﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using NUnit.Framework;

namespace Primordial.Test
{
    [TestFixture]
    public class HashTests
    {
        private const int SizeOfHashInBytes = 16;

        [Test]
        public void IsEqual()
        {
            Assert.AreEqual(Hash.Parse("00000000000000000000000000000001"), Hash.Parse("00000000000000000000000000000001"));
            Assert.AreEqual(Hash.Parse("20000000000000000000000000000000"), Hash.Parse("20000000000000000000000000000000"));
            Assert.AreNotEqual(Hash.Parse("00000000000000000000000000000001"), Hash.Parse("00000000000000000000000000000002"));
            Assert.AreNotEqual(Hash.Parse("10000000000000000000000000000000"), Hash.Parse("20000000000000000000000000000000"));
        }

        [Test]
        public void PuffTheDragon()
        {
            var guid = Guid.NewGuid();
            var bytes = guid.ToByteArray();
            var hex = string.Concat(bytes.Select(bb => bb.ToString("x2")));

            Assert.AreEqual(SizeOfHashInBytes, Marshal.SizeOf(typeof(Hash)));
            {
                Hash hash;
                Assert.AreEqual("00000000000000000000000000000000", hash.ToHexString());
                Assert.AreEqual(new byte[16], hash.ToByteArray());
            }
            {
                var hash = Hash.NewHash(bytes);
                Assert.AreEqual(bytes, hash.ToByteArray());
                Assert.AreEqual(hex, hash.ToHexString());
                Assert.AreEqual(bytes, (byte[])hash);
                Assert.AreEqual(hex, (string)hash);
            }
            {
                var hash = Hash.NewUniqueHash();
                Assert.AreEqual(SizeOfHashInBytes, ((byte[])hash).Length);
                Debug.WriteLine(hash);
            }
            {
                var hash = (Hash)guid;
                Assert.AreEqual(guid.ToByteArray(), (byte[])hash);
                Assert.AreEqual(hex, (string)hash);
                Debug.WriteLine(hash);
            }
            {
                var hash = (Hash)bytes;
                Assert.AreEqual(bytes, (byte[])hash);
                Assert.AreEqual(hex, (string)hash);
            }
            {
                var hash = Hash.Parse(hex);
                Assert.AreEqual(bytes, (byte[])hash);
                Assert.AreEqual(hex, (string)hash);
            }
        }
    }
}
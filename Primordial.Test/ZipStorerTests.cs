﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using NUnit.Framework;

namespace Primordial.Test
{
    [TestFixture]
    public class ZipStorerTests
    {
        [Test]
        public void ListFiles()
        {
            const string zipFileName = "unittest.zip";
            const string expected = "Once upon a time...";
            string actual = null;

            if (File.Exists(zipFileName)) File.Delete(zipFileName);
            using (var pk3 = ZipStorer.Create(zipFileName, "created by a unittest"))
            {
                using (var mm = new MemoryStream())
                {
                    var txt = Encoding.UTF8.GetBytes(expected);
                    mm.Write(txt, 0, txt.Length);
                    mm.Position = 0;
                    pk3.AddStream(ZipStorer.Compression.Store, "something.txt", mm, DateTime.Now, "just a test");
                }
                pk3.AddFile(ZipStorer.Compression.Store, "Primordial.Test.dll", "bin/Primordial.Test.dll", null);
            }

            using (var pk3 = ZipStorer.Open(zipFileName, FileAccess.Read))
            {
                // Read the central directory collection
                var fileEntries = pk3.ReadCentralDir();

                foreach (var zipFileEntry in fileEntries)
                {
                    Debug.Print("{0}", zipFileEntry.FilenameInZip);
                }

                if (fileEntries.Count > 0)
                {
                    var lastZipFileEntry = fileEntries[0];
                    using (var mm = new MemoryStream())
                    {
                        if (pk3.ExtractFile(lastZipFileEntry, mm))
                        {
                            Debug.Print("Streamed {0} Size={1}", lastZipFileEntry.FilenameInZip, mm.Length);
                            actual = Encoding.UTF8.GetString(mm.ToArray());
                            Debug.Print("Content '{0}'", actual);
                        }
                    }
                }
            }

            Assert.AreEqual(expected, actual);
        }
    }
}

﻿using System.Diagnostics;
using NUnit.Framework;

namespace Primordial.Test
{
    [TestFixture]
    public class ScrapsTest
    {
        private static void ShowDebugInfo()
        {
            var stackFrame = new StackFrame(1, true);

            string method = stackFrame.GetMethod().ToString();
            int line = stackFrame.GetFileLineNumber();

            Debug.Print("{0}: {1}", line, method);
        }

        [Test]
        public void StackFrameGetFileLineNumber()
        {
            ShowDebugInfo();
        }

        [Test]
        public void StackFrameMethods()
        {
            Fubar();
        }

        private static void Fubar()
        {
            var stackFrame = new StackFrame(1, true);
            Debug.Print("GetFileName = {0}", stackFrame.GetFileName());
            Debug.Print("GetFileLineNumber = {0}", stackFrame.GetFileLineNumber());
            Debug.Print("GetMethod = {0}", stackFrame.GetMethod().Name);
            Debug.Print("GetType = {0}", stackFrame.GetType());
        }
        
    }
}
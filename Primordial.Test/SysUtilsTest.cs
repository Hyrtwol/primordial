﻿using NUnit.Framework;

namespace Primordial.Test
{
    [TestFixture]
    public class SysUtilsTest
    {
        [Test]
        public void ToBinaryString()
        {
            Assert.AreEqual("101", SysUtils.ToBinaryString(5, 3));
        }

        [Test]
        public void ToHexString()
        {
            Assert.AreEqual("005", SysUtils.ToHexString(5, 3));
            Assert.AreEqual("032", SysUtils.ToHexString(50, 3));
        }
    }
}
using System;
using System.Configuration;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;

namespace Primordial
{
    public static class ConsoleUtils
    {
        public static void ConfigureConsole()
        {
            var windowSize = ConfigurationManager.AppSettings["console-size"];
            if (!string.IsNullOrEmpty(windowSize))
            {
                var vals = windowSize.Split(' ').Select(int.Parse).ToArray();
                if (vals.Length == 2)
                {
                    int height = vals[1];
                    Console.SetWindowSize(vals[0], height);
                    Console.SetBufferSize(vals[0], Math.Max(vals[1], height));
                }
            }

            var windowPosition = ConfigurationManager.AppSettings["console-position"];
            if (!string.IsNullOrEmpty(windowPosition))
            {
                var vals = windowPosition.Split(' ').Select(int.Parse).ToArray();
                if (vals.Length == 2)
                {
                    SetWindowPos(vals[0], vals[1]);
                }
            }
        }

        public static void WaitForEscape(int millisecondsTimeout = 200)
        {
            ConsoleKey key;
            do
            {
                if (Console.KeyAvailable) key = Console.ReadKey(true).Key;
                else
                {
                    key = 0;
                    Thread.Sleep(millisecondsTimeout);
                }
            } while (key != ConsoleKey.Escape);
        }

        private static readonly IntPtr ThisConsole = GetConsoleWindow();

        public static void SetWindowPos(int left, int top)
        {
            SetWindowPos(ThisConsole, 0, left, top, 0, 0, 0x0001);
        }

        [DllImport("kernel32.dll", ExactSpelling = true)]
        private static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll", EntryPoint = "SetWindowPos")]
        private static extern IntPtr SetWindowPos(IntPtr hWnd, int hWndInsertAfter, int x, int y, int cx, int cy, int wFlags);
    }
}
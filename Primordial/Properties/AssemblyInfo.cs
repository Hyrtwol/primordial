using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("Primordial")]
[assembly: AssemblyDescription("Primordial soup of code")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Hyrtwol")]
[assembly: AssemblyProduct("Primordial")]
[assembly: AssemblyCopyright("Copyright 2015 Thomas la Cour")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

[assembly: ComVisible(false)]
[assembly: Guid("9f99da1d-daac-4708-968a-51eecedb28ae")]

[assembly: AssemblyVersion("1.2.0")]
[assembly: AssemblyFileVersion("1.2.0")]

﻿using System;

namespace Primordial.Host
{
    public interface IHostService : IDisposable
    {
        string Name { get; }
        void Start();
        void Stop();
    }
}
﻿namespace Primordial.Host
{
    public class HostServiceWithWorkers : IHostService
    {
        private readonly Worker[] _workers;
        public string Name { get; }

        public HostServiceWithWorkers(params Worker[] workers)
        {
            _workers = workers;
            Name = GetType().Name;
        }

        public virtual void Start()
        {
            if (_workers == null) return;
            foreach (var worker in _workers)
            {
                StartWorker(worker);
            }
        }

        protected virtual void StartWorker(Worker worker)
        {
            worker.Start();
        }

        public virtual void Stop()
        {
            if (_workers == null) return;
            foreach (var worker in _workers)
            {
                StopWorker(worker);
            }
        }

        protected virtual void StopWorker(Worker worker)
        {
            worker.Stop();
        }

        public virtual void Dispose()
        {
            if (_workers == null) return;
            foreach (var worker in _workers)
            {
                worker.Stop();
            }
        }
    }
}
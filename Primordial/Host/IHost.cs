﻿namespace Primordial.Host
{
    public interface IHost
    {
        void Run();
    }
}